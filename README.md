# atoi_benchmark

This repository contains code for benchmarking different implementations of the
C standard library function `atoi`.

## Adding a new implementation

* create a new function in `atoi.h`
* register the function using the `ADD_FUNCTION` macro

Format the code before creating a new commit using:

```
for f in `find . -maxdepth 2 -name "*" | egrep "*\.(cpp|h)"`; do clang-format -i $f -style=webkit; done;
```

## Building and running the benchmarks

To build the project

```
mkdir build
cd build
cmake .. && make
```

Unit tests are run automatically during the build and the benchmark target is not
updated if the build fails. Benchmarking is done by using `benchmark_atoi`.

You can also get the output as a plot by using [google_benchmark_plot][1]. CMake
automatically downloads this for you and places a `plot` executable in the build
directory. For usage details please see the project's README, or you could just do

```
# benchmark the performance on individual integers
./benchmark_atoi --benchmark_format=csv --benchmark_filter=BM_int > benchmark_int.csv
./plot -f benchmark_int.csv -t inverse -r BM_int_std_atoi --xlabel 'Length of int'

# benchmark the performance on array of integers
./benchmark_atoi --benchmark_format=csv --benchmark_filter=BM_array > benchmark_array.csv
./plot -f benchmark_array.csv -t inverse -r BM_array_std_atoi --logx --xlabel 'Length of array'
```

**Note**: Plotting requires `python`, all the dependencies are automatically installed
by cmake.

[1]: https://github.com/lakshayg/google_benchmark_plot
