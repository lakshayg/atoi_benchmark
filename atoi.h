#ifndef ATOI_H_
#define ATOI_H_

// to keep intellisense happy
#ifndef ADD_FUNCTION
#define ADD_FUNCTION(func)
#endif

#include <cctype>
#include <sstream>

/////////  UTILITY FUNCTIONS  /////////////////////////////////////////////////
char const* skip_ws(char const* a)
{
    while (isspace(*a)) {
        ++a;
    }
    return a;
}

int numlen(const char* a)
{
    int len = 0;
    while (isdigit(a[len])) {
        ++len;
    }
    return len;
}

template <int n, typename T = int>
struct Pow10 {
    static const T value = 10 * Pow10<n - 1, T>::value;
};

template <typename T>
struct Pow10<0, T> {
    static const T value = 1;
};

///////////////////////////////////////////////////////////////////////////////
int std_atoi(char const* a)
{
    return std::atoi(a);
}
ADD_FUNCTION(std_atoi)

///////////////////////////////////////////////////////////////////////////////
int naive_atoi(char const* a)
{
    a = skip_ws(a);
    switch (*a) {
    case '-':
        return -naive_atoi(a + 1);
    case '+':
        return naive_atoi(a + 1);
    }
    int ans = 0;
    for (int i = 0; isdigit(a[i]); ++i) {
        ans = 10 * ans + a[i] - '0';
    }
    return ans;
}
ADD_FUNCTION(naive_atoi)

///////////////////////////////////////////////////////////////////////////////
int streams_atoi(char const* a)
{
    std::stringstream ss;
    ss << a;
    int ans;
    ss >> ans;
    return ans;
}
ADD_FUNCTION(streams_atoi)

///////////////////////////////////////////////////////////////////////////////
// Inspired by Andrei Alexandrescu's NDC London 2017 talk: Fastware
// https://www.youtube.com/watch?v=o4-CwDo2zpg

int andrei_atoi_helper(const char* a, int len)
{
    constexpr int maxlen = 8;
    if (__builtin_expect(len > maxlen, 0)) {
        return andrei_atoi_helper(a, len - maxlen) * Pow10<maxlen>::value
            + andrei_atoi_helper(a + len - maxlen, maxlen);
    }

    int sum = 0;

    // clang-format off
    switch (len) {
        case 8: sum += (a[len - 8] - '0') * Pow10<7>::value;
        case 7: sum += (a[len - 7] - '0') * Pow10<6>::value;
        case 6: sum += (a[len - 6] - '0') * Pow10<5>::value;
        case 5: sum += (a[len - 5] - '0') * Pow10<4>::value;
        case 4: sum += (a[len - 4] - '0') * Pow10<3>::value;
        case 3: sum += (a[len - 3] - '0') * Pow10<2>::value;
        case 2: sum += (a[len - 2] - '0') * Pow10<1>::value;
        case 1: sum += (a[len - 1] - '0') * Pow10<0>::value;
    }
    // clang-format on

    return sum;
}

int andrei_atoi(const char* a)
{
    a = skip_ws(a);
    if (isdigit(*a)) {
        return andrei_atoi_helper(a, numlen(a));
    } else if (*a == '-') {
        return -andrei_atoi_helper(a + 1, numlen(a + 1));
    } else if (*a == '+') {
        return andrei_atoi_helper(a + 1, numlen(a + 1));
    }
    return 0;
}
ADD_FUNCTION(andrei_atoi)
#endif