#include "benchmark/benchmark.h"
#include <array>
#include <cassert>
#include <random>

#define MAX_INT std::numeric_limits<int>::max()
#define MIN_INT std::numeric_limits<int>::min()

////// Benchmark performance on individual numbers ////////////////////////////
const std::string pos[] = {
    "",
    "9",
    "34",
    "946",
    "2605",
    "46530",
    "267405",
    "2905383",
    "23985348",
    "708347234",
};

const std::string neg[] = {
    "",
    "-4",
    "-75",
    "-275",
    "-3596",
    "-23874",
    "-238772",
    "-5876234",
    "-23762434",
    "-230462478"
};

#define ADD_FUNCTION_1(func)                                \
    static void BM_int_##func(benchmark::State& state)      \
    {                                                       \
        auto n = state.range(0);                            \
        for (auto _ : state) {                              \
            benchmark::DoNotOptimize(func(pos[n].c_str())); \
            benchmark::DoNotOptimize(func(neg[n].c_str())); \
        }                                                   \
    }                                                       \
    BENCHMARK(BM_int_##func)                                \
        ->Arg(1)                                            \
        ->Arg(2)                                            \
        ->Arg(3)                                            \
        ->Arg(4)                                            \
        ->Arg(5)                                            \
        ->Arg(6)                                            \
        ->Arg(7)                                            \
        ->Arg(8)                                            \
        ->Arg(9);

////// Benchmark performance on array of numbers //////////////////////////////
constexpr int MAX_TEST_SIZE = 1 << 25;
std::array<char[16], MAX_TEST_SIZE> A;

int intlen(int n)
{
    assert(n >= 0);
    // clang-format off
    if (n < 10) return 1;
    if (n < 100) return 2;
    if (n < 1000) return 3;
    if (n < 10000) return 4;
    return 4 + intlen(n / 10000);
    // clang-format on
}

void itoa(char* s, int n)
{
    if (n < 0) {
        *s = '-';
        return itoa(s + 1, -n);
    }
    int len = intlen(n);
    s[len] = 0;
    for (int i = len - 1; i >= 0; --i) {
        s[i] = '0' + (n % 10);
        n /= 10;
    }
}

void initialize_nums()
{
    std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_int_distribution<int> dist(MIN_INT, MAX_INT);

    for (unsigned i = 0; i < A.size(); ++i) {
        itoa(A[i], dist(rng));
    }
}

class GlobalInit {
public:
    GlobalInit()
    {
        initialize_nums();
    }
} global_init;

#define ADD_FUNCTION_2(func)                             \
    static void BM_array_##func(benchmark::State& state) \
    {                                                    \
        auto n = state.range(0);                         \
        for (auto _ : state) {                           \
            for (int i = 0; i < n; ++i) {                \
                benchmark::DoNotOptimize(func(A[i]));    \
            }                                            \
        }                                                \
    }                                                    \
    BENCHMARK(BM_array_##func)                           \
        ->Arg(1 << 4)                                    \
        ->Arg(1 << 5)                                    \
        ->Arg(1 << 6)                                    \
        ->Arg(1 << 7)                                    \
        ->Arg(1 << 9)                                    \
        ->Arg(1 << 10)                                   \
        ->Arg(1 << 11)                                   \
        ->Arg(1 << 12)                                   \
        ->Arg(1 << 13)                                   \
        ->Arg(1 << 15)                                   \
        ->Arg(1 << 17)                                   \
        ->Arg(1 << 19)                                   \
        ->Arg(1 << 21)                                   \
        ->Arg(1 << 23)                                   \
        ->Arg(1 << 25);

#define ADD_FUNCTION(func) \
    ADD_FUNCTION_1(func)   \
    ADD_FUNCTION_2(func)

#include "atoi.h"

BENCHMARK_MAIN();