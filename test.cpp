#include <iostream>
#include <limits>

#define MAX_INT std::numeric_limits<int>::max()
#define MIN_INT std::numeric_limits<int>::min()

int retval = 0;

#define ASSERT_EQUAL(a, b)                             \
    {                                                  \
        auto A = a;                                    \
        auto B = b;                                    \
        if (A != B) {                                  \
            retval = 1;                                \
            std::cerr << "=> " << #a << " = " << A     \
                      << ", expected = " << B << '\n'; \
        }                                              \
    }

#define ADD_FUNCTION(func)                              \
    class TestRunner_##func {                           \
    public:                                             \
        TestRunner_##func() { test(); }                 \
        void test()                                     \
        {                                               \
            std::cout << "* testing " << #func << '\n'; \
            ASSERT_EQUAL(func("0"), 0);                 \
            ASSERT_EQUAL(func("-1"), -1);               \
            ASSERT_EQUAL(func("1"), 1);                 \
            ASSERT_EQUAL(func("  +1"), 1);              \
            ASSERT_EQUAL(func("  1"), 1);               \
            ASSERT_EQUAL(func("  1 "), 1);              \
            ASSERT_EQUAL(func(" -1 "), -1);             \
            ASSERT_EQUAL(func("000000000000"), 0);      \
            ASSERT_EQUAL(func("000000000010"), 10);     \
            ASSERT_EQUAL(func("-00000000010"), -10);    \
                                                        \
            char buf[1 << 5];                           \
            sprintf(buf, "%d", MAX_INT);                \
            ASSERT_EQUAL(func(buf), MAX_INT);           \
                                                        \
            sprintf(buf, "%d", MIN_INT);                \
            ASSERT_EQUAL(func(buf), MIN_INT);           \
        }                                               \
    };                                                  \
    static auto var_##func = TestRunner_##func();

#include "atoi.h"

int main()
{
    return retval;
}